all: test_assign1_1

test_assign1_1: test_assign1_1.c storage_mgr.o dberror.o
	gcc -Werror -Wall -o test_assign1_1 test_assign1_1.c storage_mgr.o dberror.o
storage_mgr.o: storage_mgr.c
	gcc -Werror -Wall -c storage_mgr.c
dberror.o: dberror.c
	gcc -Werror -Wall -c dberror.c
clean:
	rm *.o test_assign1_1


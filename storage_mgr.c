//
//  storage_mgr.c
//  assignment1
//
#include "storage_mgr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void initStorageManager (void){
    return;
}
RC createPageFile (char *fileName){
    char c;
    //Check if the file is alread exist by testing open for reading
    FILE *file = fopen(fileName, "r");
    if(file!=NULL){
        printf("The file has been exsited, if you want to contine(Y) or exit(N)");
        scanf("%c",&c);
        switch(c){
            case 'Y':
                file=fopen(fileName,"w"); //Create file for writing
                break;
            case 'N':
                printf("Exit the processing!"); //Exit this processing
                exit(0);
                break;
            default:
                RC_message="Incorrect Command";
                createPageFile(fileName);
                break;
        }
    }
    else{
        file=fopen(fileName,"w"); //Create file for writing
    }
    
    //Check if the file is created successfully
    if (file==NULL){
        RC_message="File is not created";
        return RC_FILE_NOT_FOUND;
    }
    
    //To write to file and check if it is successful.
    char* buffer = (SM_PageHandle)calloc(PAGE_SIZE, sizeof(char));
    // To write a page of '\0' to fileName
    memset(buffer, '\0', PAGE_SIZE);
    size_t fw=fwrite(buffer,1,PAGE_SIZE,file);
    free(buffer);
    if (fw!=PAGE_SIZE){
        RC_message="Writing error";
        return RC_WRITE_FAILED;
    }
    fclose(file); //Close the file
    RC_message="File is created";
    return RC_OK;
}
RC openPageFile (char *fileName, SM_FileHandle *fHandle){
    FILE *file=fopen(fileName,"r+");  //Open the file
    
    //Check if the file is opened successfully
    if (file==NULL){
        RC_message="File is not exist";
        return RC_FILE_NOT_FOUND;  //If the file is not found, return error message;
    }
    fHandle->fileName=fileName; //Set fileName of the fHandle
    
    fseek(file,0,SEEK_END); //Move the pointer to end of File
    fHandle->totalNumPages = (int)(ftell(file) / PAGE_SIZE);  //set total page number
    fseek(file,0,SEEK_SET); //Move the pointer to the beginning of the file
    fHandle->curPagePos=0;  //Set current page to the beginning
    fHandle->mgmtInfo=file; //Set management info to be the file pointer.
    RC_message="File is opened";
    return RC_OK;
}
RC closePageFile (SM_FileHandle *fHandle){
    //Check if fHandle is initialized
    if (fHandle->mgmtInfo==NULL){
        RC_message="File handle is uninitialized";
        return RC_FILE_HANDLE_NOT_INIT;  //If the file handle is not initialized, return error message;
    }
    else{
        fHandle->fileName = NULL;
        //Close the file descriptor
        fclose(fHandle->mgmtInfo);
        //Set the file pointer back to NULL or free fHandle
        fHandle->mgmtInfo=NULL;
        RC_message="File is closed";
        return RC_OK;
    }
}
RC destroyPageFile (char *fileName){
    //Check if the file if removed successfully.
    if (remove(fileName)==0){
        RC_message="File is removed";
        return RC_OK;
    }
    else{
        RC_message="File is not found";
        return RC_FILE_NOT_FOUND;
    }
}
RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
     //Check if fHandle is initialized
    if (fHandle->fileName == NULL){
        RC_message="File is not found";
        return RC_FILE_NOT_FOUND;
    }
    //Check if pageNum is valid
    if (pageNum>=fHandle->totalNumPages||pageNum<0){
        RC_message="Page is not exist";
        return RC_READ_NON_EXISTING_PAGE;
    }
    
    fseek(fHandle->mgmtInfo, pageNum * PAGE_SIZE * sizeof(char), SEEK_SET); //Set the pointer to the end of pageNum-th page
    fHandle->curPagePos=pageNum+1;//Reset the current page position
    //Read block
    fread(memPage, sizeof(char), PAGE_SIZE, fHandle->mgmtInfo);
    RC_message="Block is read";
    return RC_OK;
    
}
int getBlockPos (SM_FileHandle *fHandle){
    //Check if fHandle is initialized
    if (fHandle==NULL){
        RC_message="File handle is uninitialized";
        return RC_FILE_HANDLE_NOT_INIT;
    }
    else if( fHandle->fileName == NULL){
        RC_message="File is not found";
        return RC_FILE_NOT_FOUND;
    }
    else{
        return fHandle->curPagePos;
    }
}
RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    return readBlock(0,fHandle,memPage);

}
RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    return readBlock(fHandle->totalNumPages-1,fHandle,memPage);

}
RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    return readBlock(fHandle->curPagePos-1,fHandle,memPage);
}
RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    return readBlock(fHandle->curPagePos,fHandle,memPage);
}
RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    return readBlock(fHandle->curPagePos+1,fHandle,memPage);
    
}
RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
    //Check if fHandle is initialized
    if (fHandle==NULL){
        RC_message="File handle is uninitialized";
        return RC_FILE_HANDLE_NOT_INIT;
    }
    if ( fHandle->fileName == NULL) {
        RC_message="File is not found";
        return RC_FILE_NOT_FOUND;
    }
     //Check if pageNum is valid
    if (pageNum>=fHandle->totalNumPages||pageNum<0){
        RC_message="Page is not exist";
        return RC_READ_NON_EXISTING_PAGE;
    }
    fseek((FILE *)fHandle->mgmtInfo,pageNum*PAGE_SIZE * sizeof(char),SEEK_SET);  //Set the pointer to the end of pageNum-th page
    //Check if write is successful
    if(fwrite(memPage,1,PAGE_SIZE,(FILE *)fHandle->mgmtInfo)!=PAGE_SIZE){
        RC_message="Block written is failed";
        return RC_WRITE_FAILED;
    }
    else{
        RC_message="Block is written";
        return RC_OK;
    }
}
RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    return writeBlock(fHandle->curPagePos,fHandle,memPage);
}
RC appendEmptyBlock (SM_FileHandle *fHandle){
    //Check if fHandle is initialized
    if ( fHandle->fileName == NULL) {
        RC_message="File is not found";
        return RC_FILE_NOT_FOUND;
    }
    if (fHandle==NULL){
        RC_message="File handle is uninitialized";
        return RC_FILE_HANDLE_NOT_INIT;
    }
    fseek((FILE *)fHandle->mgmtInfo,0,SEEK_END);  //Move the pointer to the end of the file
    char* buffer = (SM_PageHandle)calloc(PAGE_SIZE, sizeof(char));
    memset(buffer, '\0', PAGE_SIZE);
    //Check if write is successful
    size_t fw=fwrite(buffer,1,PAGE_SIZE,(FILE *)fHandle->mgmtInfo);
    free(buffer);
    if (fw!=PAGE_SIZE){
        RC_message="Writing error";
        return RC_WRITE_FAILED;
    }
    else{
        fHandle->totalNumPages++;
        RC_message="Block appended";
        return RC_OK;
    }
}
RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle){
    //Check if fHandle is initialized
    if (fHandle==NULL){
        RC_message="File handle is uninitialized";
        return RC_FILE_HANDLE_NOT_INIT;
    }
    else if (fHandle->totalNumPages >= numberOfPages){
        return RC_OK;
    }
    else{
        int i;
        for (i=fHandle->totalNumPages; i<numberOfPages; i++){
            appendEmptyBlock(fHandle);
        }
        RC_message="Capacity is ensured";
        return RC_OK;
    }
}


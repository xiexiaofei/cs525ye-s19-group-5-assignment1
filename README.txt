How to compile and run our code:

Go to the folder of assignment

Makefile: Compile the program

storage_mgr.c: implementation of the method

storage_mgr.h: definition and deliberation of the method

test_assign1_1.c: added two lines to make test program memory safe: free(ph);
                              TEST_CHECK(closePageFile(&fh));

#: We make comments on every method and important implementation step


By: Group # 5, which include:
	Hanlin Tian: htian7@hawk.iit.edu
	Yuhang Yang: yyang179@hawk.iit.edu
	Xiaofei Xie: xxie18@hawk.iit.edu


Contribution: Each member in our group do the project equally.
